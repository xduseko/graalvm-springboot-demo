# see https://docs.spring.io/spring-boot/docs/3.2.x/reference/html/native-image.html#native-image.advanced.converting-executable-jars.native-image

FROM docker.io/library/maven:3.9-eclipse-temurin-21 as maven
WORKDIR /app
COPY pom.xml ./
RUN mvn -P native dependency:go-offline
COPY src/ ./src/
RUN mvn -P native clean package && \
    (cd target && ln -s graalvm-springboot-demo-*.jar app.jar)

FROM ghcr.io/graalvm/graalvm-community:22 as graalvm
WORKDIR /app
COPY native-image.argfile ./
COPY --from=maven /app/target/app.jar ./
RUN jar -xvf app.jar && \
    native-image -H:Name=app @native-image.argfile -cp .:BOOT-INF/classes:$(find BOOT-INF/lib | tr '\n' ':')

FROM docker.io/library/alpine:3.19
RUN apk add --no-cache gcompat && \
    adduser --system app
USER app
WORKDIR /app
COPY --from=graalvm /app/app ./
CMD ./app
