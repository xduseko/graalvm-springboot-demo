package com.example.demo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;

@RestController
@RequestMapping("/now")
public class NowController {

    @GetMapping
    public LocalDateTime now() {
        return LocalDateTime.now();
    }

}
